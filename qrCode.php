<?php
require 'aws/aws-autoloader.php';
include 'qr/qrlib.php';
include 'configVars.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

$publication = $_POST['publication_name'];
$title = $_POST['article_chapter_title'];
$date = $_POST['date'];
$access_token = $_POST['access_token'];

$data = array(
    'Name'        => $publication."-".$title."-".$date,
    'UrlCode'        => str_replace(" ","",$publication."-".$title)."-".generateRandomString()
);

$options = array(
    CURLOPT_RETURNTRANSFER  => true,   // return web page
    CURLOPT_FOLLOWLOCATION  => true,   // follow redirects
    CURLOPT_POST            => true,
    CURLOPT_POSTFIELDS      => $data,
    CURLOPT_HTTPHEADER      => array("application/x-www-form-urlencoded")
);

$ch = curl_init("http://".SITE_DOMAIN."/api/v2/categories?access_token=".$access_token);
curl_setopt_array($ch, $options);

try{
    $response = json_decode(curl_exec($ch));
}
catch(Exception $e){
    echo $e->getMessage();
    exit();
}

if(isset($response->status)){
    echo $response->message;
    exit();
}

//API REQUEST
$codeContents = $response->url;
$fileName = md5($publication."-".$title."-".$date).".png";
$urlRelativeFilePath = EXAMPLE_TMP_URLRELPATH.$fileName;

QRcode::png($codeContents, $urlRelativeFilePath,"L",4,4);

$s3 = new S3Client([
    'version' => 'latest',
    'region'  => 'ca-central-1'
]);

try {
    // Upload data.
    $result = $s3->putObject([
        'Bucket' => "qrdiscuss",
        'Key'    => $fileName,
        'SourceFile' => $urlRelativeFilePath,
        'ACL'    => 'public-read',
        'ContentType' => "image/png",
    ]);
    $result['ObjectURL'];
    unlink($urlRelativeFilePath);
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

echo json_encode(['qrImage' => $result['ObjectURL'], 'discussion_url' => $response->url],JSON_UNESCAPED_SLASHES);
